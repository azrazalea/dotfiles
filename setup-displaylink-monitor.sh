#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

xrandr --setprovideroutputsource 1 0
xrandr --output DVI-I-1-1 --auto --left-of eDP-1
