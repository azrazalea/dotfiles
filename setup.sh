#!/bin/bash
# Should only be run from within dotfiles repo due to stupid pwd checking
# Doesn't deal with pre-existing files/directories
START_DIR=`pwd`

cd $HOME

ln -s $START_DIR/.Xresources ./
ln -s $START_DIR/.emacs.d ./
ln -s $START_DIR/.gnupg ./
ln -s $START_DIR/.lein ./
ln -s $START_DIR/.sbclrc ./
ln -s $START_DIR/.eclrc ./
ln -s $START_DIR/.ccl-init.lisp ./
ln -s $START_DIR/.stumpwm.d ./
ln -s $START_DIR/.xinitrc ./
ln -s $START_DIR/.authinfo.gpg ./
ln -s $START_DIR/.bash_profile ./
ln -s $START_DIR/.bashrc ./
ln -s $START_DIR/.zshrc ./
ln -s $START_DIR/.latexmkrc ./
ln -s $START_DIR/gcl-info ./
ln -s $START_DIR/.fonts.conf ./
ln -s $START_DIR/.unison ./
ln -s $START_DIR/.gitconfig ./
ln -s $START_DIR/.gnupg ./
mkdir -p $HOME/.local/share
if [ ! -e $HOME/.local/share/fonts ]; then ln -s $START_DIR/fonts $HOME/.local/share/fonts; fi
ln -s ./.xinitrc ./.xsession
ln -s $START_DIR/.ansible ./
ln -s $START_DIR/.ansible.cfg ./
